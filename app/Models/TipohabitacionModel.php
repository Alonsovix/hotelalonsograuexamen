<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Models;
use CodeIgniter\Model;

/**
 * Description of TipohabitacionModel
 *
 * @author a026636126x
 */
class TipohabitacionModel extends Model{
    protected $table = 'tipohabitacion';
    protected $primaryKey = 'id';
    protected $returnType = 'object';
    protected $allowedFields = ['nombre', 'descripcion', 'tecnologia', 'capacidad', 'adultos', 'ninyos'];
    
    protected $validationRules = [
        'nombre'    => 'required',
        'capacidad'    => 'required',
    ];
}
