<?= $this->extend('templates/default') ?>

//Disponemos de 3 secciones
<?= $this->section('head_title') ?>
    <?= $title?>
<?= $this->endSection() ?>

<?= $this->section('title') ?>
    <?= $title?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>

    <ul class="nav justify-content-end mb-4">
        <li class="nav-item ">
            <a class="nav-link active btn btn-primary" href="<?=site_url('insertar/habitacion')?>">Insertar</a>
        </li>
    </ul>
    <table class="table table-striped" id="myTable">
        <thead>
            <th>
                Nombre
            </th>
            <th>
                Descripcion
            </th>
            <th>
                Tecnologia
            </th>
            <th>
                Capacidad
            </th>
        </thead>
        <tbody>
        <?php foreach ($habitaciones as $habitacion): ?>
            <tr>
                <td>
                    <?= $habitacion->nombre ?>
                </td>
                <td>
                    <?= $habitacion->descripcion ?> 
                </td>
                <td>
                    <?= $habitacion->tecnologia ?>
                </td>
                <td>
                    <?= $habitacion->capacidad ?>
                </td>
                <td>
                    <ul class="nav justify-content-end mb-4">
        <li class="nav-item ">
            <a class="nav-link active btn btn-primary" href="<?=site_url('editar/habitacion')?>">Editar</a>
        </li>
    </ul>
                </td>
             
               
                
            </tr>
        <?php endforeach; ?>
        </tbody>    
    </table>

<script>
    function ventanita() {  
        alert("Se va a eliminar la habitacion, estas seguro?");  
    }  
</script>
<?= $this->endSection() ?>


