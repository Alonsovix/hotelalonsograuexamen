<?= $this->extend('templates/default') ?>

//Disponemos de 3 secciones
<?= $this->section('head_title') ?>
    <?= $title?>
<?= $this->endSection() ?>

<?= $this->section('title') ?>
    <?= $title?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
    <?php if (!empty($errores)): ?>
        <div class="alert alert-danger">
            <?php foreach ($errores as $field => $error): ?>
                <p><?=$field?>:<?= $error ?></p>
            <?php endforeach ?>
        </div>
    <?php endif ?>

    <form action="<?= site_url('insertar/creahabitacion')?>" method="post">
        <div class="form-group">
            <?= form_label('nombre:', 'nombre', ['class'=>'col-2'])?>
            <?= form_input('nombre',set_value('nombre',''),['class'=>'form_control col-9', 'id'=>'nombre']) ?>
        </div>
        <div class="form-group">
            <?= form_label('descripcion:', 'descripcion', ['class'=>'col-2 col-form-label'])?>
            <?= form_input('descripcion',set_value('descripcion',''),['class'=>'form_control col-9', 'id'=>'descripcion']) ?>
        </div>
        <div class="form-group">
            <?= form_label('tecnologia:', 'tecnologia', ['class'=>'col-2 col-form-label'])?>
            <?= form_input('tecnologia',set_value('tecnologia',''),['class'=>'form_control col-9', 'id'=>'tecnologia']) ?>
        </div>
        <div class="form-group">
            <?= form_label('capacidad', 'capacidad', ['class'=>'col-2 col-form-label'])?>
            <?= form_input('capacidad',set_value('capacidad',''),['class'=>'form_control col-9', 'id'=>'capacidad']) ?>
        </div>
        <div class="form-group">
            <?= form_label('adultos:', 'adultos', ['class'=>'col-2 col-form-label'])?>
            <?= form_input('adultos',set_value('adultos',''),['class'=>'form_control col-9', 'id'=>'adultos']) ?>
        </div>
        <div class="form-group">
            <?= form_label('ninyos', 'ninyos', ['class'=>'col-2 col-form-label'])?>
            <?= form_input('ninyos',set_value('ninyos',''),['class'=>'form_control col-9', 'id'=>'ninyos']) ?>
        </div>
        <input class="btn btn-primary" type="submit" name="enviar" value="Enviar" />
    </form>
<?= $this->endSection() ?>

