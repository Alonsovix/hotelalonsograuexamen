<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;
use App\Models\TipohabitacionModel;
/**
 * Description of TipohabitacionController
 *
 * @author a026636126x
 */
class TipohabitacionController extends BaseController {
    
    public function listahabitaciones(){
        /*
        $TipohabitacionModel = new TipohabitacionModel();
        echo '<pre>';
        print_r($alumnoModel->findAll());
        echo '</pre>';
        $data['title'] = 'Listado de Habitaciones';
        $data['habitaciones'] = $TipohabitacionModel->findAll();
        return view('hotel/lista',$data); */
        
        $data['title'] = 'Listado de Habitaciones';
        $TipohabitacionModel = new TipohabitacionModel ();
        $data['habitaciones'] = $TipohabitacionModel->findAll();
        return view('hotel/lista', $data);
    }
    public function formhabitacion() {
        helper('form');
        $data['title'] = 'Insertar Habitacion';
        return view('hotel/formhabitacion', $data);
        
        
    }
    
    public function crearhabitacion() {
        $habitacion_nueva = $this->request->getPost();
        
        //echo '<pre>';
        //print_r($grupo_nuevo);
        //echo '</pre>';
        
        $TipohabitacionController = new TipohabitacionModel();
        $TipohabitacionController->insert($habitacion_nueva);
        return redirect()->to('habitaciones/lista');
        
        
    }

    public function borrarhabitacion ($id) {
        $TipohabitacionModel = new TipohabitacionModel();
        $data['id'] = $TipohabitacionModel->where('id', $id)->delete();
        return  redirect()->to('listado/grupos', $data);
     }
     
     public function editarhabitacion($id){
        helper('form');
        $TipohabitacionModel = new TipohabitacionModel();
        $data['title'] = 'Modificar Habitacion';
        $data['nombre'] = $TipohabitacionModel->find($id);
        echo '<pre>';
        print_r($data);
         echo '</pre>';
        //return view('hotel/formhabitacion',$data);
         
         
    }
    
    public function habitacioneditada($id){
        
        helper('form');
        $TipohabitacionModel = new TipohabitacionModel();
        $data['title'] = 'Modificar Habitacion';
        $data['nombre'] = $TipohabitacionModel->find($id);
        $grupo = $this->request->getPost();
        unset($grupo['botoncito']);
        /*
        echo '<pre>';
             print_r($grupo);
        echo '</pre>';
        */
        
        $TipohabitacionModel->update($id,$nombre);
        
        if ($TipohabitacionModel->update($id,$nombre)=== false){
            $data['errores'] = $TipohabitacionModel->errors();
            $data['title'] = 'Modificar Habitacion';
            return view('hotel/formhabitacion',$data);
        }
        
        return redirect('hotal/lista');
    }
    }

